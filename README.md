# TestAzureCi

[![Build Status](https://dev.azure.com/real0793/TestAzureCI/_apis/build/status/TestAzureCI-CI?branchName=master)](https://dev.azure.com/real0793/TestAzureCI/_build/latest?definitionId=2&branchName=master)

[![Build Status](https://dev.azure.com/real0793/TestAzureCI/_apis/build/status/TestAzureCI-CI?branchName=dev)](https://dev.azure.com/real0793/TestAzureCI/_build/latest?definitionId=2&branchName=dev)

This repo is created for tests and experiments with Azure pipelines.

Contains a simple application "Hello world!" and unit tests based on GTest which builds via VCPKG manager.

[![Build Status](https://dev.azure.com/real0793/TestAzureCI/_apis/build/status/TestAzureCI-CI?branchName=dev)](https://dev.azure.com/real0793/TestAzureCI/_build/latest?definitionId=2)