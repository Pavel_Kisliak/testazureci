﻿// TestAzureCI.cpp : Defines the entry point for the application.
//

#include "TestAzureCI.h"
#include <string>
#include <string_view>
#include <type_traits>

using namespace std;

template <typename TChar>
void TestCxx17(basic_string_view<TChar> str)
{
	if constexpr (std::is_same_v<TChar, char>)
	{
		cout << str << endl;
	}
	else
	{
		wcout << str << endl;
	}
}

int main()
{
	const std::string str = "Hello World!";
	TestCxx17<char>(str);
	return 0;
}
