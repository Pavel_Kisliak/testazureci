/*******************************************************************************
* Copyright (C) 2018 by Pavel Kisliak                                          *
* This file is part of BitSerializer library, licensed under the MIT license.  *
*******************************************************************************/

#include "gtest/gtest.h"


TEST(TestGtests, TestSuccessTest)
{
	// Arrange
	EXPECT_TRUE(true);
}

TEST(TestGtests, TestFailTest)
{
	// Arrange
	EXPECT_TRUE(false);
}

TEST(TestGtests, TestFailOnlyInDebug)
{
#ifdef _DEBUG
	EXPECT_TRUE(false);
#else
	EXPECT_TRUE(true);
#endif
}
